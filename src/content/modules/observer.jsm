// vim: set ft=javascript

var EXPORTED_SYMBOLS = ["Observer"];

function log(aText) {
  var console = Components.classes["@mozilla.org/consoleservice;1"]
      .getService(Components.interfaces.nsIConsoleService);
  console.logStringMessage(aText);
};

//create an nsIObserver implementor
var Observer = {
    _RE_CONVO_URL       : new RegExp('api.sling.is/conversations/(\\d+)/messages'), //  Where we are POSTing our texts.
    _RE_MSG_WINDOW_URL  : new RegExp('sling.is/messages/(\\d+)/.+'),                //  When we need to know if the browser is looking at a Sling convo.
    _conversation_ids   : Array(),                                                //  The list of convo ids for which each convo is translated.
    _is_observing       : false,

    isObserving: function(state) {
        /* Returns true if the Observer is observing, else False */
        /* If state is true or false, the value is set */
        if( state === true )
            this._is_observing = true;
        if( state === false )
            this._is_observing = false;
        return this._is_observing;
    },

    addConvoID : function(cid) {
        /* Pre : cid is a string for a Sling conversation ID
         * Post: Messages you post to this conversation will be translated from en to ru.
         */
        if (this._conversation_ids.indexOf(cid) == -1)
            this._conversation_ids.push(cid);
    },

    removeConvoID : function(cid) {
        /* Pre : cid is an ID for a convo already being translated
         * Post: the convo with this cid is no longer translated
         */
        var idx = this._conversation_ids.indexOf(cid);

        if (idx > -1)
            if (this._conversation_ids.length == 1)
                this._conversation_ids.pop();
            else
                this._conversation_ids = this._conversation_ids.splice(idx, 1);
    },

    getConvoIDs : function() {
        /* Returns the array of conversations registered with the Observer */
        return this._conversation_ids;
    },

    observe : function(subject, topic, data) {
        // Main observer callback
        if( topic == "http-on-modify-request" )
            this._handle_outgoing(subject, topic, data);
        else
            log('Unsupported topic: ' + topic);
    },

    _handle_outgoing : function(subject, topic, data) {
        var channel = subject.QueryInterface(Components.interfaces.nsIHttpChannel);
        var url     = subject.URI.spec;

        //if( this._RE_CONVO_URL.test(url) && channel.requestMethod == "POST")
        if( this.isUrlBeingObserved(url, false) && channel.requestMethod == "POST")
        {
            // Get the POST data
            var uploadChannel = channel.QueryInterface(Components.interfaces.nsIUploadChannel);  
            var uploadChannelStream = uploadChannel.uploadStream;  
            uploadChannelStream.QueryInterface(Components.interfaces.nsISeekableStream).seek(Components.interfaces.nsISeekableStream.NS_SEEK_SET, 0);  

            var stream = Components.classes["@mozilla.org/binaryinputstream;1"].createInstance(Components.interfaces.nsIBinaryInputStream);  
            stream.setInputStream(uploadChannelStream);  

            var postBytes = stream.readByteArray(stream.available());  
            var poststr = String.fromCharCode.apply(null, postBytes);  

            var cb = function(poststr, channel, uploadChannel) {
                var inputStream = Components.classes["@mozilla.org/io/string-input-stream;1"].createInstance(Components.interfaces.nsIStringInputStream);
                inputStream.setData(poststr, poststr.length);
                uploadChannel.setUploadStream(inputStream, "application/json;charset=utf-8", -1);
                channel.requestMethod = "POST";  // order important - setUploadStream resets to PUT
            };

            this._outbound_translate_wrapper(poststr, 'en', 'ru', cb, channel, uploadChannel);
        }
    },

    isUrlBeingObserved : function(url, is_from_urlbar) {
        /* Params:
         *     - url is a string
         *     - is_from_urlbar is a bool. It should be true iff the url is obtained
         *       from the urlbar as opposed to pointing to the Sling API.
         * Returns:
         *     - a boolean with value true if this url is being observed else false
         */
        var _RE_URL;

        if ( is_from_urlbar == true )
            _RE_URL = this._RE_MSG_WINDOW_URL;
        else
            _RE_URL = this._RE_CONVO_URL;

        if( _RE_URL.test(url) )
        {
            var cid = _RE_URL.exec(url)[1];
            if (this.getConvoIDs().indexOf(cid) > -1)
                return true;
        }
        return false;
    },

    _refine_translated : function(r)
    {
        // Returns json from translated.
        r = r.split(']]',1)[0];
        r = r.replace(',,','');
        r = r.replace('[,','[');
        r = r.slice(1);
        r = r + ']]';
        return JSON.parse(r);
    },

    _outbound_translate_wrapper : function(json, source, target, callback, channel, uploadChannel)
    {
        var d = JSON.parse(json);
        var r = this._refine_translated(this._askGoogle(d.content, source, target));
        var t = r[r.length -1][0];  // PROBLEM: Encoding for russian chars wtf
        callback(JSON.stringify({'content':t}), channel, uploadChannel); 
    },

    translateIncoming : function(text, source, target)
    {
        var r = this._askGoogle(text, source, target);
        var re_cyrillic = new RegExp('\\[,".+?",').exec(r);
        r = this._refine_translated(r);
     
        if( re_cyrillic != null ) {
            log('cyrillic detected');
            var x = re_cyrillic[0];
            x = x.slice(3, -2);
            r = this._askGoogle(x, source, target);
            r = this._refine_translated(r);
        }
        return r[0][0];
    },

    _askGoogle : function(text, source, target)
    {
        /* Params:
         *     - (string)  text is the string to be translated
         *     - (string)  source is the code for the origin language
         *     - (string)  target is the code for the target language
         * Returns:
         *     - (string) Google Translate's response
         */
        var link   = "https://translate.google.com/translate_a/single?client=t&sl="+source+"&tl="+target+"&hl=en&dt=bd&dt=ex&dt=ld&dt=md&dt=qc&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&ssel=3&tsel=0&otf=1&tk=517146|642290&q=" + text;
        var oReq   = Components.classes["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();
        var retval = null;

        oReq.onload = function() {
            if(oReq.readyState == 4)
                retval = oReq.response;
        }

        oReq.open("get", link, false); //  true for blocking - we must block.
        oReq.send();
        return retval;
    },

    QueryInterface : function(aIID) {
        if (aIID.equals(Components.interfaces.nsISupports) || aIID.equals(Components.interfaces.nsIObserver))
            return this;
        throw Components.results.NS_NOINTERFACE;
    }

};

