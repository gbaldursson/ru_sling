/* Manager for conversations to be used with conversations.xul. Holds a list of convo ids and enables/disables the Observer. */

Components.utils.import('chrome://rusling/content/modules/observer.jsm');

if ("undefined" == typeof(ConversationsManager)) {
  var ConversationsManager = {};
};

var observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);

ConversationsManager.setup = function()
{
    /* Draws the window based on Observer's state. */
    var lbox   = document.getElementById('existing_convos_list');
    var radios = document.getElementById('rad_is_enabled');
    var convos = Observer.getConvoIDs(); 

    for( var i = 0; i < convos.length; i++ )
        lbox.appendItem(convos[i]);

    if( Observer.isObserving() )
        radios.selectedIndex = 0;
    else
        radios.selectedIndex = 1;
};

ConversationsManager.AddConversation = function()
{
    /* Adds the conversation selected in the list to observation */
    var tbox = document.getElementById('textbox-convo-id');
    var lbox = document.getElementById('existing_convos_list');
    var cid  = tbox.value;

    if(parseInt(cid))
    {
        Observer.addConvoID(cid);
        lbox.appendItem(cid);
        tbox.reset();
    }
};

ConversationsManager.RemoveConversation = function()
{ 
    /* Removes the conversation selected in the list from observation */
    var lbox = document.getElementById('existing_convos_list');
    var item = lbox.selectedItem;
    var idx  = lbox.getIndexOfItem(item);

    if( item )
    {
        var cid = item.label;
        Observer.removeConvoID(cid); 
        lbox.removeItemAt(idx);
    }
};

ConversationsManager.StartObserver = function()
{
    if( !Observer.isObserving() )
    {
        observerService.addObserver(Observer, "http-on-modify-request", false);
        Observer.isObserving(true);
        //window.addEventListener("mouseover", this.mouse_handler);
        //
        // Adding an event listener here does not work. I suspect it is because
        // this object ceises to exist when xul window closes as opposed to being retained
        // in memory.
        //
    }
};

ConversationsManager.StopObserver = function()
{
    if( Observer.isObserving() )
    {
        observerService.removeObserver(Observer, "http-on-modify-request");
        Observer.isObserving(false);
        window.removeEventListener("mouseover", this.mouse_handler);
    }
};

ConversationsManager.openConvoWindow = function(e)
{
    window.open("chrome://rusling/content/conversations.xul", "conversation-window", "chrome,centerscreen");
};

ConversationsManager.mouse_handler = function(e) {
    /* Purpose: When the mouse hovers over an observed conversation, a red border 
     *          is put around it and when user presses the hotkey the text gets translated
     * Params:  e is the event
     */

    // Known bug: One can't click on messages and have this work with the last convo. Instead one must click
    // the convo in question specifically so that the url becomes like: 
    // "https://sling.is/messages/359/gunnarr-baldursson"

    // 0. Are we observing?
    if( !Observer.isObserving() )
        return;

    // Borrowed from http://stackoverflow.com/questions/11594576/getting-current-browser-url-in-firefox-addon
    var windowsService = Components.classes['@mozilla.org/appshell/window-mediator;1'].getService(Components.interfaces.nsIWindowMediator);
    var currentWindow = windowsService.getMostRecentWindow('navigator:browser');
    var browser = currentWindow.getBrowser();
    var uri = browser.currentURI;
    var url = uri.spec;    
        
    //  1. Is this a Sling convo under observation?
    if( Observer.isUrlBeingObserved(url, true) ) 
        //  2. Is the mouse over a received text message?
        if( e.target.tagName === 'SPAN' && e.target.getAttribute('ng-bind-html') && !e.target.getAttribute('rusling') ) {
            e.target.setAttribute('rusling', 'yes');
            console.log('Original: '+e.target.innerHTML);
            var translation = Observer.translateIncoming(e.target.innerHTML, 'ru', 'en');
            console.log('Changed: '+translation);
            e.target.innerHTML = translation;
        }
};

// WARNING: Global event listener.
window.addEventListener("click", ConversationsManager.mouse_handler);
// TODO: Support removal of event listener. This requires more memory management stuff.
